import java.util.Scanner;
public class MethodenImFahrkartenautomat {
	static Scanner tastatur = new Scanner(System.in);
	public static void main(String[] args) {
		double zuZahlenderBetrag = fahrkartenbestellungErfassen();
//	int ticketAnzahl = ticketgrenzen();
		double r�ckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
		fahrkartenAusgaben();
		rueckgeldAusgeben(r�ckgabebetrag);

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
				"vor Fahrtantritt entwerten zu lassen!\n"+
				"Wir w�nschen Ihnen eine gute Fahrt.");
	}
	public static double fahrkartenbestellungErfassen() {

		double zuZahlenderBetrag; 
		System.out.print("Zu zahlender Betrag (EURO): ");
		zuZahlenderBetrag = tastatur.nextDouble();

		return zuZahlenderBetrag;
	}
	public static int ticketgrenzen() {
		
		System.out.println("Wie viele Tickets kaufen Sie?:");
		int tickets = tastatur.nextInt();
		if (tickets <= 10 || tickets >= 1) {
			System.out.println("Die Ticketanzahl wurde best�tigt");	
		}
		else {
			tickets = 1;
			System.out.println("Es k�nnen nur minimal 1 und maximal 10 Tickets gekauft werden!");
		}
		return tickets;
	}
	
	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {

		double eingezahlterGesamtbetrag;

		eingezahlterGesamtbetrag = 0.0;
		while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
		{
			System.out.printf("Noch zu zahlen: %.2f Euro\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			double eingeworfeneM�nze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}
		tastatur.close();
		return eingezahlterGesamtbetrag - zuZahlenderBetrag;
	}
	public static void fahrkartenAusgaben() {
	       System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");
	}
	public static void rueckgeldAusgeben(double r�ckgabebetrag) {

		
		if(r�ckgabebetrag > 0.0)
		{
			System.out.println("Der R�ckgabebetrag in H�he von " + r�ckgabebetrag + " EURO");
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
			{
				System.out.println("2 EURO");
				r�ckgabebetrag -= 2.0;
			}
			while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
			{
				System.out.println("1 EURO");
				r�ckgabebetrag -= 1.0;
			}
			while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
			{
				System.out.println("50 CENT");
				r�ckgabebetrag -= 0.5;
			}
			while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
			{
				System.out.println("20 CENT");
				r�ckgabebetrag -= 0.2;
			}
			while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
			{
				System.out.println("10 CENT");
				r�ckgabebetrag -= 0.1;
			}
			while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
			{
				System.out.println("5 CENT");
				r�ckgabebetrag -= 0.05;
			}
		}


		tastatur.close();

	}
}

