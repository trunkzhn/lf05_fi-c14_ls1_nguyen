
public class Mittelwert {

	public static void main(String[] args) {

		// (E) "Eingabe"
		// Werte f�r x und y festlegen:
		// ===========================
		double x = 2.0;
		double y = 4.0;
		double m = 	berechneMittelwert(x, y);
		System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);

	}
		// (A) Ausgabe
		// Ergebnis auf der Konsole ausgeben:
		// =================================
		static double berechneMittelwert(double a, double b) {
			double result = (a + b) / 2.0;
			return result;
		}
	}

