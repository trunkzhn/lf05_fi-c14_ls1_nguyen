import java.util.Scanner;

public class Noten {
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Geben Sie bitte Ihre Note ein.");
		int note = scan.nextInt();
		
		
		if(note == 1) {
			System.out.println("Sehr gut");
		}
		else if(note == 2){
			System.out.println("Gut");
		}
		else if(note == 3) {
			System.out.println("Befriedigend");
		}
		else if(note == 4) {
			System.out.println("Ausreichend");
		}
		else if(note == 5) {
			System.out.println("Mangelhaft");
		}
		else if(note == 6) {
			System.out.println("Ungenügend");
		}
		scan.close();
	}
}
