import java.util.Scanner;

public class Kennenlernen {
	public static void main(String [] args) {

		// Neues Scanner-Objekt myScanner wird erstellt
		Scanner myScanner = new Scanner(System.in);

		System.out.print("Guten Tag, wie hei�en Sie?");
		
		// Die Variable name speichert die erste EIngabe
		String name = myScanner.nextLine();
		
		System.out.println("Hallo " + name + ", wie alt bist du?");
		
		// D/e Variable alter speichert die zweite Eingabe
		int alter = myScanner.nextInt();
		
		// Ausgabe der Daten
		System.out.println(name + ", " + alter + " Jahre alt, ihr Name steht nun auf der Liste vom FBI, sie haben 24 Stunden");
		// Scanner Schlie�ung
		myScanner.close();
	}

}
