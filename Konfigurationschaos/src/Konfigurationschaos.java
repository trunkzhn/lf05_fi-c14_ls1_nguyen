
public class Konfigurationschaos {

	public static void main(String[] args) {
		{
			{

				int 	euro;
				int 	cent;
				int 	summe;
				double  fuellstand;
				String  name;
				char sprachModul = 'd';
				boolean statusCheck;
				String bezeichnung = "Q2021_FAB_A";
				String typ = "Automat AVR";
			
				final byte PRUEFNR = 4;
				double maximum = 100.00;
				double patrone = 46.24;
				int muenzenCent = 1280;
				int muenzenEuro = 130;

				name = typ + " " + bezeichnung;

				summe = muenzenCent + muenzenEuro * 100;
				fuellstand = maximum - patrone;
				euro = summe / 100;
				cent = summe % 100;


				statusCheck = (euro <= 150) 
						&& (euro >= 50)
						&& (cent != 0)
						&& (sprachModul == 'd')
						&& (fuellstand >= 50.00) 
						&&  (!(PRUEFNR == 5 || PRUEFNR == 6)); 
				System.out.println("Name: " + name);
				System.out.println("Sprache: " + sprachModul);
				System.out.println("Pr�fnummer : " + PRUEFNR);
				System.out.println("F�llstand Patrone: " + fuellstand + " %");
				System.out.println("Summe Euro: " + euro +  " Euro");
				System.out.println("Summe Rest: " + cent +  " Cent");		
				System.out.println("Status: " + statusCheck);

			}

		}
	}
}